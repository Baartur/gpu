
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <iostream>
using namespace std;

// LDLt factorization from exercise 3 chapter 2 of lecture notes
// Only one thread for each system
// Parameters:  - a: concatenation of all values of the N matrices
//              - y: concatenation of all value vectors
//              - d: matrix index (n is in [1,N]), number of y elements stored in the shared

__global__ void LDLt_k(float *a, float *y, int d){

    // thread identifier in the grid
    int tidx = threadIdx.x + blockIdx.x*blockDim.x;
    // shared memory
    extern __shared__ float sA[];
    
    // Local integers
    int i, j, k, n2, n2p1;

    n2 = (d*d+d)/2; // number of elements from A stored in the shared
    n2p1 = n2+d; // number of known elements stored in the shared for each system

    // Copy the lower triangular part from global to shared memory
    for (i=0;i<d;i++){
        for (j=0;j<=i;j++){
            sA[threadIdx.x * n2p1 + i*(i+1)/2+j] = a[tidx*d*d+i*d+j]; 
            // stock  in the shared memory all we need to resolve 1 linear system
            // we use 1 thread per system, so each thread  trade n2p1 elements of the shared
            
            //i(i+1)/2 : number of elements of line i-1 from A stored in the shared
            // before storing elements of line i, we need to jump from i(i+1)/2 elements
        }
    }

    // Copy the value vector from the global to shared memory
    for (i=0;i<d;i++){
        sA[threadIdx.x * n2p1 + n2 + i] = y[tidx*d + i]; // store y, then A (ie after copying d(d+1)/2=n2 elements from the of the lower triangular part of A
    }    

    // Perform the LDLt factorization
    for (i=0;i<d;i++){
        for (j=0;j<i;j++){
            // Mat[i*Dim+j] /= Mat[j*Dim+j]
            // Aij <- A_ij/D_jj ? On the first pass i=0 et j=0 so Aij/Ajj=A00/A00=1 which corresponds to A_ij/D_jj
            // and if Ajj=D_jj so thiis is true
            sA[threadIdx.x * n2p1 + i*(i+1)/2+j] /= sA[threadIdx.x * n2p1 + j*(j+1)/2+j];
            for (k=0;k<j;k++){
                //Aij <-  Aij - Dkk*Lik*Ljk/Djj
                sA[threadIdx.x * n2p1 + i*(i+1)/2+j] -= sA[threadIdx.x * n2p1 + k*(k+1)/2+k]*
                                                        sA[threadIdx.x * n2p1 + i*(i+1)/2+k]*
                                                        sA[threadIdx.x * n2p1 + j*(j+1)/2+k] /
                                                        sA[threadIdx.x * n2p1 + j*(j+1)/2+j];

            }// At the end : Aij <- Lij
        }

        for (k=0;k<i;k++){
            // Dii computed by iteration : Aii <- Aii - D_kk*L_ik^2
            sA[threadIdx.x * n2p1 + i*(i+1)/2+i] -= sA[threadIdx.x * n2p1 + k*(k+1)/2+k]*
                                                    sA[threadIdx.x * n2p1 + i*(i+1)/2+k]*
                                                    sA[threadIdx.x * n2p1 + i*(i+1)/2+k];
                
        }// At the end : Aii <- D_ii
    }
    // Resolve the system using LDLt factorization
    for (i=0;i<d;i++){  
        for (k=0;k<j;k++){
            sA[threadIdx.x * n2p1 + n2+i] -= sA[threadIdx.x * n2p1 + i*(i+1)/2+k]*
                                            sA[threadIdx.x * n2p1 + n2+k];
        } 
    }

    for (i=d-1;i>=0;i--){
        sA[threadIdx.x * n2p1 +n2+i] /= sA[threadIdx.x * n2p1 + i*(i+1)/2+i];
        for (k=i+1;k<d;k++) {
            sA[threadIdx.x * n2p1 + n2+i] -= sA[threadIdx.x * n2p1 + k*(k+1)/2+i]*
                                            sA[threadIdx.x * n2p1 + n2+k];            
        }
    }

    // Copy the solution vector from shared to global Memory
    for (i=0;i<d;i++){
        y[tidx*d+i] = sA[threadIdx.x*n2p1+n2+i];
    }

}


// LDLt_max_k kernel: perform batch LDLt factorization on GPU by row calculation
// For d <= 64, there is enough shared memory for all the data of a linear system
// So we copy all the data to the shared memory to accelerate the calculation
__global__ void LDLt_max_k(float *A, float *y, int d) {
    int tidx = threadIdx.x % d; // Index of the thread in a sub block of size d
    int Qt = (threadIdx.x - tidx) / d; // Index of the sub block containing d threads
    int gbx = Qt + blockIdx.x * (blockDim.x / d); // Index of the sub block in the grid
    
    extern __shared__ float sA[]; // shared memory to accelarate the computation

    int i, k, grid, nt, n2;

    grid = d*(d+1)/2 + d; 
    n2 = grid; // number of known elements by system (we stored all the values of A in the shared)
    nt = Qt * grid; 

    // The d threads in a grid parallelly collect matrix and vector from global to shared memory
    // for each iteration, take the exact number of threads to copy one row of the matrix need d iterations
    for (i = d; i > 0; i--) {
        if (tidx < i) {
            sA[nt + n2 - i*(i+1)/2 + tidx] = A[gbx*d*d + (d-i)*d + tidx + d-i];
        }
    }
    sA[nt + tidx] = y[gbx*d + tidx];
    __syncthreads(); // Wait for the collection finishing


    // Perform the LDLt factorization
    for (i = d; i > 0; i--) {
        // The first thread in a grid computes D_ii for all i in (ici) [1,d]
        if (tidx == 0) {
            for (k = d; k > i; k--) { //we compute D_ii 
                //A_ii = sA[nt + n2 - i*(i+1)/2] 
                //D_kk = sA[nt + n2 - k*(k+1)/2] 
                //L_ik = sA[nt + n2 - k*(k+1)/2 + k - i]
                sA[nt + n2 - i*(i+1)/2] -= sA[nt + n2 - k*(k+1)/2] * sA[nt + n2 - k*(k+1)/2 + k - i] * sA[nt + n2 - k*(k+1)/2 + k - i];
            }
        }
        __syncthreads(); // Wait for finishing D_i computation

        // The first i-1 threads in a grid compute L_ji
        if (tidx < i-1) {
            sA[nt + n2 - i*(i+1)/2 + tidx + 1] /= sA[nt + n2 - i*(i+1)/2];
            for (k = d; k > i; k--) {
                sA[nt + n2 - i*(i+1)/2 + tidx + 1] -= sA[nt + n2 - k*(k+1)/2] * sA[nt + n2 - k*(k+1)/2 + k - i] * sA[nt + n2 - k*(k+1)/2 + tidx + 1 + k - i] / sA[nt + n2 - i*(i+1)/2];
            }
        }
        __syncthreads(); // Wait for finishing L_ji computation
    }

    // Solving the linear system using LDLt factorization
    for (i = d-1; i > 0; i--) {
        if (tidx < i) {
            sA[nt + d - i + tidx] -= sA[nt + d - i - 1] * sA[nt + n2 - (i+1)*(i+2)/2 + tidx + 1];
        }
        __syncthreads(); // Waiting for the previous finishing
    }
    sA[nt + tidx] /= sA[nt + n2 - (d-tidx)*(d-tidx+1)/2];
    __syncthreads(); // Waiting for all threads finishing
    for (i = 1; i < d; i++) {
        if (tidx < d-i) {
            sA[nt + tidx] -= sA[nt + d - i] * sA[nt + n2 - (d-tidx)*(d-tidx+1)/2 + d-tidx-i];
        }
        __syncthreads(); // Waiting for the previous finishing
    }
    y[gbx*d + tidx] = sA[nt + tidx];
}



// LDLt_max_col_k kernel: perform batch LDLt factorization on GPU by column calculation
// In theory, this will be a little slower than the row version
// Because the closer threads do not read closer cases in the global memory
// In practice, as d <= 64 is small and we access to global memory only once for copying data
// and the data are stored the same in shared memory, so there is no great difference
__global__ void LDLt_max_col_k(float *A, float *y, int d) {
    int tidx = threadIdx.x % d; 
    int Qt = (threadIdx.x - tidx) / d; 
    int gbx = Qt + blockIdx.x * (blockDim.x / d); 

    extern __shared__ float sA[]; // shared memory to accelarate the computation

    int i, k, grid, nt, n2;

    grid = d*(d+1)/2 + d;
    n2 = grid;
    nt = Qt * grid;

    // The d thread in a sub-block parallelly collect matrix and vector from global to shared memory
    for (i = d; i > 0; i--) {
        if (tidx < i) {
            sA[nt + n2 - i*(i+1)/2 + tidx] = A[gbx*d*d + (tidx + d-i)*d + d-i]; // change tidx*d
        }
     }
    sA[nt + tidx] = y[gbx*d + tidx];
    __syncthreads(); // Wait for the collection finishing


    // Perform the LDLt factorization
    for (i = d; i > 0; i--) {
        // The first thread in a grid computes D_i
        if (tidx == 0) {
            for (k = d; k > i; k--) {
                sA[nt + n2 - i*(i+1)/2] -= sA[nt + n2 - k*(k+1)/2] * sA[nt + n2 - k*(k+1)/2 + k - i] * sA[nt + n2 - k*(k+1)/2 + k - i];
            }
        }
        __syncthreads(); // Wait for finishing D_i computation

        // The first i-1 threads in a grid compute L_ji
        if (tidx < i-1) {
            sA[nt + n2 - i*(i+1)/2 + tidx + 1] /= sA[nt + n2 - i*(i+1)/2];
            for (k = d; k > i; k--) {
                sA[nt + n2 - i*(i+1)/2 + tidx + 1] -= sA[nt + n2 - k*(k+1)/2] * sA[nt + n2 - k*(k+1)/2 + k - i] * sA[nt + n2 - k*(k+1)/2 + tidx + 1 + k - i] / sA[nt + n2 - i*(i+1)/2];
            }
        }
        __syncthreads(); // Wait for finishing L_ji computation
    }

    // Solving the linear system using LDLt factorization
    for (i = d-1; i > 0; i--) {
        if (tidx < i) {
            sA[nt + d - i + tidx] -= sA[nt + d - i - 1] * sA[nt + n2 - (i+1)*(i+2)/2 + tidx + 1];
        }
        __syncthreads(); // Waiting for the previous finishing
    }
    sA[nt + tidx] /= sA[nt + n2 - (d-tidx)*(d-tidx+1)/2];
    __syncthreads(); // Waiting for all threads finishing
    for (i = 1; i < d; i++) {
        if (tidx < d-i) {
            sA[nt + tidx] -= sA[nt + d - i] * sA[nt + n2 - (d-tidx)*(d-tidx+1)/2 + d-tidx-i];
        }
        __syncthreads(); // Waiting for the previous finishing
    }
    y[gbx*d + tidx] = sA[nt + tidx];
     
}


int main(int argc,      // Number of strings in array argv
        char *argv[]    // Array of command-line argument strings
    ){
    std::string arg = argv[1]; //store cmd argument

    float meanTime = 0.0;
    int nbTry = 25;
    int l;

    for (l=0; l<nbTry; l++){
        int i, j, k;
        // The rank of the matrix
        int Dim=16;
        // The minimum number of threads per block
        int minTB = 32;
        // The number of blocks
        int NB = 16384;
        // The number of matrices to invert
        int size = NB*minTB;
        // Paramter to fill the matrices
        float rho;
        // The matrix and the value vector
        float *A, *AGPU, *Y, *YGPU;
    
        float TimerAddOne;
        cudaEvent_t start, stop;
        cudaEventCreate(&start);
        cudaEventCreate(&stop);
    
        // Memory allocation
        A = (float *)calloc(size*Dim*Dim, sizeof(float));
        Y = (float *)calloc(size*Dim, sizeof(float));
        cudaMalloc(&AGPU, size*Dim*Dim*sizeof(float));
        cudaMalloc(&YGPU, size*Dim*sizeof(float));
    
        // Setting values
        for (i=0;i<size; i++){ //i is the indice of the system
            rho = 1.0f/(1.1f+i);
            for (j=0;j<Dim;j++){
                for (k=0;k<Dim;k++){
                    if (j==k){
                        A[i*Dim*Dim+j*Dim+k] = 1.0f; //1.0 on the diagonal 
                    }
                    else {
                        A[i*Dim*Dim+j*Dim+k] = rho; //1.0/(1.1+i) otherwise
                    }
                }
                Y[j+i*Dim] = 0.5f*j; 
    
            }
        }

        cudaMemcpy(AGPU, A, size*Dim*Dim*sizeof(float), cudaMemcpyHostToDevice);
        cudaMemcpy(YGPU, Y, size*Dim*sizeof(float), cudaMemcpyHostToDevice);
        // GPU timer isntructions
        cudaEventRecord(start, 0);
    
        // Resolve the systems AX=Y using LDLt factorization
        if (arg=="non_collab"){
            LDLt_k<<<NB, minTB, minTB*((Dim*Dim+Dim)/2+Dim)*sizeof(float)>>>(AGPU, YGPU, Dim); //non collaborative version
        }else if(arg=="row_collab"){
            LDLt_max_k<<<NB,Dim*minTB,minTB*((Dim*Dim+Dim)/2+Dim)*sizeof(float)>>>(AGPU, YGPU, Dim); //collaborative version by row
        }else if(arg=="col_collab"){
            LDLt_max_col_k<<<NB,Dim*minTB,minTB*((Dim*Dim+Dim)/2+Dim)*sizeof(float)>>>(AGPU, YGPU, Dim); //collaborative version by column
        }

        // LDLt_k<<<NB, minTB, minTB*((Dim*Dim+Dim)/2+Dim)*sizeof(float)>>>(AGPU, YGPU, Dim); //non collaborative version
        // LDLt_max_k<<<NB,Dim*minTB,minTB*((Dim*Dim+Dim)/2+Dim)*sizeof(float)>>>(AGPU, YGPU, Dim); //collaborative version by row
        // LDLt_max_col_k<<<NB,Dim*minTB,minTB*((Dim*Dim+Dim)/2+Dim)*sizeof(float)>>>(AGPU, YGPU, Dim); //collaborative version by column
    
        cudaEventRecord(stop,0); // GPU timer instructions
        cudaEventSynchronize(stop); // GPU timer instructions
        cudaEventElapsedTime(&TimerAddOne, start, stop); // GPU timer instructions
        cudaMemcpy(Y, YGPU, size*Dim*sizeof(float), cudaMemcpyDeviceToHost); 
    
        // check a numerical value
        i = 79;
        for (j=0;j<Dim;j++) printf("%f \n", Y[j+i*Dim]);
    
        printf("GPU Timer: %f ms\n", TimerAddOne);
        meanTime += TimerAddOne;

    
        // Memory free
        free(A);
        cudaFree(AGPU);
        cudaFree(YGPU);
        cudaFree(YGPU);
        cudaEventDestroy(start); // GPU timer instructions
        cudaEventDestroy(stop); // GPU timer instructions
    }
    meanTime /= nbTry;
    printf("Mean time over %d tries: %f ms\n", nbTry, meanTime);
    return 0;   
}