# Batch LDLt factorization

A batch LDLt factorization for the project of GPU Programming at ENSAE Paris.

## Project instruction

Instructions can be found in folder <code>instructions</code>

## Project structure

Our work will be presented in a PDF file available before May 21th.

The code is runable in the main notebook <code>GPU_LDLT_ATTIA_ARTUR.ipynb</code>

Source code is contained in folder <code>src</code> in cuda file <code>projet_gpu.cu</code>

## Run project
Possibility 1: follow instructions in <code>GPU_LDLT_ATTIA_ARTUR.ipynb</code> that you can find [here](https://colab.research.google.com/drive/119pKcutQPCHe6SOyIerEF_AJ-8ap6lwZ?usp=sharing)

Possibility 2: Run these instructions from command line

- cd project
<code>cd gpu </code>
- compile code
<code>nvcc ./src/projet_gpu.cu -o ldlt </code>
- run non collaborative version
<code>./ldlt non_collab</code>
- run row implementation
<code>./ldlt row_collab</code>
- run column implementation
<code>./ldlt col_collab</code>